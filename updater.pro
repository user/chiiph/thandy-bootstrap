TEMPLATE = app
TARGET = bin/updater
DEPENDPATH += . src src/ui
INCLUDEPATH += . src

macx:LIBS += -framework Carbon

# Input
HEADERS += src/ThandyWrapper.h \
           src/UpdaterDialog.h \
           src/ProcessList.h \
           src/DownloadWidget.h
SOURCES += src/main.cpp \
           src/ThandyWrapper.cpp \
           src/UpdaterDialog.cpp \
           src/ProcessList.cpp

FORMS += src/ui/maindialog.ui \
         src/ui/download.ui

RESOURCES += src/res.qrc
RCC_DIR = src/

MOC_DIR = moc_obj
OBJECTS_DIR = moc_obj

UI_DIR = src/ui
