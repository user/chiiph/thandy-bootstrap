/* Copyright (c) 2011, The Tor Project, Inc. */
/* See LICENSE for licensing information */

#ifndef THANDYWRAPPER_H
#define THANDYWRAPPER_H

#include <QtGui>

class ThandyWrapper : public QObject
{
 Q_OBJECT

 public:
  /** Default constructor. Creates the settings object with the value
      from conf plus "/updater.conf" */
  ThandyWrapper(const QString &conf = ".", bool docheck = true);
  /** Destructor */
  ~ThandyWrapper();

  /** Returns true if it is only checking for updates */
  bool justCheck() { return docheck; }
  /** Checks for updates, and downloads them if download is true */
  void check(bool download);
  /** Updates the configured bundles */
  void update();

 signals:
  /** Emitted when there is a new output message */
  void outputMessage(const QString &msg);
  /** Emitted when the Thandy process has terminated */
  void done();

 private slots:
  /** Outputs the messages to stdin if docheck is true, otherwise it
      emits outputMessage(msg) */
  void fillOutput();
  /** Finishes reading the buffer if the Thandy process is done and
      there still are lines to read */
  void doneReadAll();

 private:
  QTimer timer; /** < Timer to check on the output buffer */
  QSettings *_settings; /** < Settings pointer to handle configuration
                            options */
  QProcess p; /** < Thandy process */
  bool docheck; /** < Boolean value that is true if it is only
                    checking for updates */

  /** Returns the configure bundles in the format that is needed to be
      an argument for Thandy */
  QString getBundles();
  /** Runs the thandy process for bundles. It downloads and installs
      these depending on the value of download and install */
  void update(const QString &bundles, bool download, bool install = false);
};

#endif
