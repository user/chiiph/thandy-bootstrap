/* Copyright (c) 2011, The Tor Project, Inc. */
/* See LICENSE for licensing information */

#include "ThandyWrapper.h"

#include <iostream>

ThandyWrapper::ThandyWrapper(const QString &conf, bool ch) :
  QObject(), docheck(ch)
{
  _settings = new QSettings(QString("%1/updater.conf").arg(conf),
                            QSettings::IniFormat);
}

ThandyWrapper::~ThandyWrapper()
{
  p.kill();
}

void
ThandyWrapper::check(bool download)
{
  update(getBundles(), download);
}

void
ThandyWrapper::update()
{
  update(getBundles(), true, true);
}

void
ThandyWrapper::fillOutput()
{
  if(!p.canReadLine())
    return;

  QString out = p.readLine(500);

  if(out.size() == 0)
    return;

  if(docheck) {
    std::cout << out.toAscii().data();
  } else {
    emit outputMessage(out);
  }
}

void
ThandyWrapper::doneReadAll()
{
  while(p.canReadLine())
    fillOutput();
}

void
ThandyWrapper::update(const QString &bundles, bool download, bool install)
{
  QProcessEnvironment env = QProcessEnvironment::systemEnvironment();

  env.insert("PYTHONPATH", _settings->value("PythonPath", "").toString());
  env.insert("THP_DB_ROOT", _settings->value("ThpDbRoot", "").toString());
  env.insert("THP_INSTALL_ROOT", _settings->value("ThpInstallRoot", "").toString());

  p.setProcessEnvironment(env);

  p.setReadChannel(QProcess::StandardError);

  connect(&timer, SIGNAL(timeout()), this, SLOT(fillOutput()));
  timer.start(10);

  QString cmd = QString("%1 %2/lib/thandy/ClientCLI.py update --controller-log-format %3 %4 %5")
    .arg(_settings->value("PythonBinary", ".").toString())
    .arg(_settings->value("ThandyLocation", ".").toString())
    .arg(download?"":"--no-download")
    .arg(install?"--install":"")
    .arg(bundles);
  qWarning() << cmd;
  p.start(cmd);

  connect(&p, SIGNAL(finished(int, QProcess::ExitStatus)), this, SIGNAL(done()));
  connect(&p, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(doneReadAll()));
}

QString
ThandyWrapper::getBundles()
{
  return _settings->value("Bundles", "").toStringList().join(" ");
}
