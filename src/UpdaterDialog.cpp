/* Copyright (c) 2011, The Tor Project, Inc. */
/* See LICENSE for licensing information */

#include "UpdaterDialog.h"
#include "ProcessList.h"
#include "DownloadWidget.h"

#if defined(Q_WS_MAC)
#  include <Carbon/Carbon.h>
#endif

UpdaterDialog::UpdaterDialog(ThandyWrapper *th, const QString &_restartCmd, QWidget *parent)
  : QDialog(parent), thandy(th), updating(false), lastLabel(0), restartCmd(_restartCmd)
{
  ui.setupUi(this);

// Not implemented for now
  ui.lblAsk->setVisible(false);
  ui.btnTerminate->setVisible(false);

  connect(thandy, SIGNAL(outputMessage(const QString &)),
          this, SLOT(displayMessage(const QString &)));
  connect(thandy, SIGNAL(done()),
          this, SLOT(done()));

  if(isSomethingRunning()) {
    ui.stackedWidget->setCurrentIndex(pgProcess);
  } else {
    ui.stackedWidget->setCurrentIndex(pgUpdate);
    ui.btnOk->setEnabled(false);

    updating = true;
    startUpdate();
    updating = false;
  }

  statusLayout = qobject_cast<QVBoxLayout*>(ui.statusContent->layout());
  downloadLayout = qobject_cast<QVBoxLayout*>(ui.downloadContent->layout());

#if defined(Q_WS_MAC)
  // Put the process in foreground on mac
  ProcessSerialNumber psn = { 0, kCurrentProcess };
  TransformProcessType(&psn, kProcessTransformToForegroundApplication);
#endif
}

UpdaterDialog::~UpdaterDialog()
{
  if(!restartCmd.isEmpty())
    QProcess::startDetached(restartCmd);
  delete thandy;
}

bool
UpdaterDialog::isSomethingRunning()
{
  ProcessList pl;
  QHash<quint64, QString> plist(pl.get());
  bool something = false;

  foreach(quint64 pid, plist.keys()) {
    if(plist.value(pid).contains("App/Firefox/firefox-bin", Qt::CaseInsensitive) or
       plist.value(pid).contains("MacOS/Firefox.app", Qt::CaseInsensitive) or
       plist.value(pid).contains("tbb-firefox.exe", Qt::CaseInsensitive)) {
      QListWidgetItem *it = new QListWidgetItem(QIcon(":/firefox"),
                                                QString("Firefox (%1)").arg(pid));
      ui.listWidget->addItem(it);
      something = true;
    } else if(plist.value(pid).contains("App/vidalia", Qt::CaseInsensitive) or
       plist.value(pid).contains("MacOS/Vidalia.app", Qt::CaseInsensitive) or
       plist.value(pid).contains("vidalia.exe", Qt::CaseInsensitive)) {
      QListWidgetItem *it = new QListWidgetItem(QIcon(":/vidalia"),
                                                QString("Vidalia (%1)").arg(pid));
      ui.listWidget->addItem(it);
      something = true;
    } else if(plist.value(pid).contains("App/tor", Qt::CaseInsensitive) or
       plist.value(pid).contains("MacOS/tor", Qt::CaseInsensitive) or
       plist.value(pid).contains("tor.exe", Qt::CaseInsensitive)) {
      QListWidgetItem *it = new QListWidgetItem(QIcon(":/tor"),
                                                QString("Tor (%1)").arg(pid));
      ui.listWidget->addItem(it);
      something = true;
    }
  }

  return something;
}

void
UpdaterDialog::startUpdate()
{
  thandy->update();
}

void
UpdaterDialog::closeEvent(QCloseEvent *ev)
{
  if(updating) {
    QMessageBox::StandardButton res =
      QMessageBox::question(this,
                            "Do you want to close?",
                            "You are about to close the Updater "
                            "while you are in the middle of an update.\n"
                            "Do you really want to do it?",
                            QMessageBox::Yes | QMessageBox::No,
                            QMessageBox::No);
    if(res == QMessageBox::Yes) {
      ev->accept();
    } else {
      ev->ignore();
    }
  }
}

void
UpdaterDialog::displayMessage(const QString &msg)
{
  processMessage(msg);
}

void
UpdaterDialog::addStatus(const QString &msg)
{
  if(ui.lblNoMsg->isVisible())
    ui.lblNoMsg->setVisible(false);
  
  if(lastLabel)
    lastLabel->setEnabled(false);

  statusLayout->insertWidget(0, lastLabel = new QLabel(msg));
}

void
UpdaterDialog::addDownload(const QString &name, int size, int downloaded)
{
  if(ui.lblNoDownload->isVisible())
    ui.lblNoDownload->setVisible(false);

  DownloadWidget *d = new DownloadWidget(name, size, downloaded);
  downloadHash.insert(name, d);
  downloadLayout->insertWidget(0, d);
  if(size == downloaded)
    d->downloadFinished();
}

const QString
UpdaterDialog::processMessage(const QString &msg)
{
  QPair<MsgType, QStringList> res = parseMessage(msg);

  int size = 0;
  int downloaded = 0;
  QString name = "";
  DownloadWidget *d = 0;
  
  switch(res.first) {
  case INFO:
    // if(res.second.size() > 0)
    //   addStatus(res.second.at(0));
    break;
  case ERROR:
    addStatus(QString("<font color=\"%1\">ERROR: %2 script has failed.</font>")
              .arg("#ff0000")
              .arg(res.second.at(0)));
    break;
  case DOWNLOAD:
    size = res.second.at(0).toInt();
    downloaded = res.second.at(1).toInt();
    name = res.second.at(2);
    if(downloadHash.contains(name)) {
      d = downloadHash.value(name, 0);
      d->update(downloaded);
      if(size == downloaded)
        d->downloadFinished();
    } else {
      qWarning() << "Adding" << name << size << downloaded;
      addDownload(name, size, downloaded);
    }
    break;
  case READY:
    addStatus(QString("<font color=\"%1\">Ready to install %2</font>")
              .arg("#298F00")
              .arg(res.second.at(0)));
    if(!thandy->justCheck())
      addStatus(QString("<font color=\"%1\">Starting %2 installation...</font>")
                .arg("#298F00")
                .arg(res.second.at(0)));
    break;
  case WANTFILE:
    addStatus(QString("<font color=\"%1\">Starting download for file: %2</font>")
              .arg("#0588BC")
              .arg(res.second.at(0)));
    break;
  case WAIT:
    addStatus(QString("<font color=\"%1\">Waiting for %2</font>")
              .arg("#298f00")
              .arg(res.second.at(0)));
    break;
  case ALREADY:
    addStatus(QString("<font color=\"%1\">Bundle already installed: %2</font>")
              .arg("#298f00")
              .arg(res.second.at(0)));
    break;
  }
  return msg;
}

const QPair<UpdaterDialog::MsgType, QStringList>
UpdaterDialog::parseMessage(const QString &msg)
{
  int ind = msg.indexOf(" ");
  if(ind == -1)
    return QPair<MsgType, QStringList>(INFO, QStringList() << msg);

  QString type_str = msg.left(ind);
  MsgType type = INFO;
  if(type_str == "ERROR")
    type = ERROR;
  else if(type_str == "DOWNLOAD")
    type = DOWNLOAD;
  else if(type_str == "READY")
    type = READY;
  else if(type_str == "WANTFILE")
    type = WANTFILE;
  else if(type_str == "WAIT")
    type = WAIT;
  else if(type_str == "ALREADY")
    type = ALREADY;

  QString rest = msg.right(msg.size() - ind - 1);
  int eqpos, fquote, squote;
  eqpos = fquote = squote = 0;
  
  int from = 0;
  QStringList parts;
  while((eqpos = rest.indexOf("=", from)) != -1) {
    fquote = rest.indexOf("\"", eqpos);
    squote = rest.indexOf("\"", fquote + 1);
    from = squote + 1;
    parts << rest.mid(fquote + 1, squote - fquote - 1);
  }

  return QPair<MsgType, QStringList>(type, parts);
}

void
UpdaterDialog::done()
{
  addStatus(QString("<font color=\"%1\">Done.</font>")
            .arg("#0588BC"));
  ui.btnOk->setEnabled(true);
}
