/* Copyright (c) 2011, The Tor Project, Inc. */
/* See LICENSE for licensing information */

#include "ProcessList.h"

#if defined(Q_WS_MAC)
#include <Carbon/Carbon.h>
#endif

#if defined(Q_WS_WIN32)
#include <QLibrary>

#include <tlhelp32.h>
#include <shlobj.h>

#if defined(UNICODE)
/* Force the ascii verisons of these functions, so we can run on Win98. We
 * don't pass any Unicode strings to these functions anyway. */
#undef PROCESSENTRY32
#undef LPPROCESSENTRY32
#undef Process32First
#undef Process32Next
#endif

/* Load the tool help functions dynamically, since they don't exist on
 * Windows NT 4.0 */
typedef HANDLE (WINAPI *CreateToolhelp32Snapshot_fn)(DWORD, DWORD);
typedef BOOL (WINAPI *Process32First_fn)(HANDLE, LPPROCESSENTRY32);
typedef BOOL (WINAPI *Process32Next_fn)(HANDLE, LPPROCESSENTRY32);
#endif

ProcessList::ProcessList()
{}

ProcessList::~ProcessList() {}

const QHash<quint64, QString>
ProcessList::get()
{
#if defined(Q_WS_X11)
  return x11_get();
#elif defined(Q_WS_WIN32)
  return win32_get();
#elif defined(Q_WS_MAC)
  return mac_get();
#endif
}

#if defined(Q_WS_X11)
const QHash<quint64, QString>
ProcessList::x11_get()
{
  QProcess proc;
  proc.start("ps -ax -o pid= -o cmd=");

  if(proc.waitForFinished()) {
    QHash<quint64, QString> list;

    QStringList output(QString(proc.readAllStandardOutput()).split("\n"));
    QStringList parts;
    foreach(QString line, output) {
      parts = line.split(" ", QString::SkipEmptyParts);
      if(parts.size() < 2)
        continue;
      if(parts[1].startsWith("["))
        continue;

      list.insert(parts[0].toUInt(), parts[1]);
    }

    return list;
  }

  return QHash<quint64, QString>();
}
#endif

#if defined(Q_WS_WIN32)
const QHash<quint64, QString>
ProcessList::win32_get()
{
  QHash<quint64, QString> procList;
  CreateToolhelp32Snapshot_fn pCreateToolhelp32Snapshot;
  Process32First_fn pProcess32First;
  Process32Next_fn pProcess32Next;
  HANDLE hSnapshot;
  PROCESSENTRY32 proc;
  QString exeFile;
  qint64 pid;

  /* Load the tool help functions */
  pCreateToolhelp32Snapshot =
    (CreateToolhelp32Snapshot_fn)QLibrary::resolve("kernel32", "CreateToolhelp32Snapshot");
  pProcess32First = (Process32First_fn)QLibrary::resolve("kernel32", "Process32First");
  pProcess32Next = (Process32Next_fn)QLibrary::resolve("kernel32", "Process32Next");
 
  if (!pCreateToolhelp32Snapshot || !pProcess32First || !pProcess32Next) {
    qWarning("Unable to load tool help functions. Running process information "
             "will be unavailable.");
    return QHash<quint64, QString>();
  }

  /* Create a snapshot of all active processes */
  hSnapshot = pCreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  if (hSnapshot != INVALID_HANDLE_VALUE) {
    proc.dwSize = sizeof(PROCESSENTRY32);
    
    /* Iterate through all the processes in the snapshot */
    if (pProcess32First(hSnapshot, &proc)) {
      do {
        /* Extract the PID and exe filename from the process record */
        pid = (qint64)proc.th32ProcessID;
        exeFile = QString::fromAscii((const char *)proc.szExeFile);
        
        /* Add this process to our list */
        qWarning() << pid << exeFile;
        procList.insert(pid, exeFile);
      } while (pProcess32Next(hSnapshot, &proc));
    }
    CloseHandle(hSnapshot);
  }
  return procList;
}
#endif

#if defined(Q_WS_MAC)
const QHash<quint64, QString>
ProcessList::mac_get()
{
  // ProcessSerialNumber psn = { 0, kNoProcess };
  // ProcessInfoRec info;
  // info.processInfoLength = sizeof(ProcessInfoRec);
  // info.processName = nil;

  // while (noErr == GetNextProcess(&psn)) {
  //   pid_t pid;
  //   if (noErr == GetProcessPID(&psn, &pid)) {
  //     qWarning() << "AAA";
  //     qWarning() << pid;
  //     if(noErr == GetProcessInformation(&psn, &info))
  //       qWarning() << pid;
  //     qWarning() << "BBB";
  //   }
  // }

  // QHash<quint64, QString> h;
  // h.insert(0, "App/Firefox/firefox-bin");
  // return h;
  QProcess proc;
  proc.start("ps -ax -o pid= -o comm=");

  if(proc.waitForFinished()) {
    QHash<quint64, QString> list;

    QStringList output(QString(proc.readAllStandardOutput()).split("\n"));
    QStringList parts;
    foreach(QString line, output) {
      parts = line.split(" ", QString::SkipEmptyParts);
      if(parts.size() < 2)
        continue;

      list.insert(parts[0].toUInt(), parts[1]);
    }

    return list;
  }

  return QHash<quint64, QString>();
}
#endif

